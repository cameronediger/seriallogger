﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Configuration;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace SerialLogger
{
	class Program
	{
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		static bool _bRunning = true;
		static SerialPort _serialPort = null;

		static void Main(string[] args)
		{
			//if args == ? or help
			//display options.
			log.Info("Starting Serial Logger...");
			Thread readThread = new Thread(ReadPort);

			_serialPort = new SerialPort();
			_serialPort.PortName = ConfigurationManager.AppSettings["PortName"].ToString();
			_serialPort.BaudRate = Convert.ToInt32(ConfigurationManager.AppSettings["BaudRate"].ToString());
			_serialPort.Parity = (Parity)Enum.Parse(typeof(Parity), ConfigurationManager.AppSettings["Parity"].ToString());
			_serialPort.DataBits = Convert.ToInt32(ConfigurationManager.AppSettings["DataBits"].ToString());
			_serialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), ConfigurationManager.AppSettings["StopBits"].ToString());
			_serialPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), ConfigurationManager.AppSettings["Handshake"].ToString());

			_serialPort.ReadTimeout = 500;
			_serialPort.WriteTimeout = 500;

			log.Info("Opening Serial Port: " + _serialPort.PortName);
			_serialPort.Open();
			_bRunning = true;
			log.Info("Starting ReadThread");
			readThread.Start();

			Console.WriteLine("Type 'quit' to exit");
			while (_bRunning)
			{
				string cmd = Console.ReadLine();
				if (cmd.ToLower().CompareTo("quit") == 0)
				{
					_bRunning = false;
				}
			}

			log.Info("Exiting...");
			readThread.Join();
			_serialPort.Close();

			log.Info("Done!");
			Console.WriteLine("DONE!");
			Console.ReadLine();
		}

		public static void ReadPort()
		{
			log.Info("ReadPort Thread started");
			while (_bRunning)
			{
				try
				{
					string strMsg = _serialPort.ReadLine();
					Console.WriteLine(strMsg);
					log.Info(strMsg);
				}
				catch (TimeoutException) { }
			}

			log.Info("Exiting ReadPort Thread");
		}

		static void DisplayHelp()
		{
			string[] serialPorts = SerialPort.GetPortNames();
 			Console.WriteLine("Serial Ports Found: ");
			foreach (string port in serialPorts)
			{
				Console.WriteLine(port);
			}
			
			//Console.WriteLine("\nBaudRate({0})", sp.BaudRate);
			//Console.WriteLine("\nParity({0}): ", sp.Parity.ToString());
			foreach(string s in Enum.GetNames(typeof(Parity)))
			{
				Console.WriteLine("  {0}", s);
			}

			//Console.WriteLine("\nDataBits({0})", sp.DataBits);
			//Console.WriteLine("\nStopBits({0})", sp.StopBits.ToString());
			foreach(string s in Enum.GetNames(typeof(StopBits)))
			{
				Console.WriteLine("  {0}", s);
			}

			//Console.WriteLine("\nHandshake({0})", sp.Handshake.ToString());
			foreach(string s in Enum.GetNames(typeof(Handshake)))
			{
				Console.WriteLine("  {0}", s);
			}			
		}
	}
}
